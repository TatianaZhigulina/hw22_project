function personShow(id) {
  let x = setInterval(function() {loading()}, 100);
  fetch("/civilian/" + id).then(response => response.json()).then(
  data => {
    document.getElementById("loadingForm").remove();
    document.getElementById("name").innerHTML = `NAME: ${data.firstName} ${data.secondName}`;
    document.getElementById("name").setAttribute("id", "nameLoaded");
    document.getElementById("age").innerHTML = `AGE: ${data.age}`;
    document.getElementById("occupation").innerHTML = `OCCUPATION: ${data.occupation.toLowerCase()}`;
    document.getElementById("editButton").href = "/edit/" + id;
  });
}

function personDelete() {
  if (confirm("You wanna delete an item?")) {
    fetch("/civilian/" + document.person, {method: "DELETE"}).then(x => window.location.replace("/"));
  }
}

function popup() {
let params = "scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=800,left=100,top=100";
open('/greeting', 'POPUP', params);
}

function getBred(length) {
  r = ""
  for (let i=0; i<=length; i++) {
    r += (Math.random() + 1).toString(36).substring(2);
  }
  return r;
}
function loading() {
  if (document.getElementById("loadingString")) {
    document.getElementById("loadingString").innerHTML = getBred(5);
  } else {clearInterval(document.x)}
}
