package com.webVillage.app.Framework.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webVillage.app.Framework.pages.BasePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestHelper {

    @LocalServerPort
    private Integer port;

    public String basePath;
    public JavascriptExecutor js;
    public TakesScreenshot ts;

    public static WebDriver driver;
    private static Map<String, Object> data;

    @BeforeAll
    public static void setProperty() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        data = mapper.readValue(
                new File("C:\\Users\\Zhigu\\Java_course\\lecture_31\\webFun\\src\\test\\java\\com\\webVillage\\app\\Framework\\config\\baseChrome.json"),
                Map.class
        );
        if ((data.containsKey("chromeDriver"))) {
            System.setProperty("webdriver.chrome.driver", (String) data.get("chromeDriver"));
        }
    }

    @BeforeEach
    public void getReady() throws IOException, JSONException {
        buildDriver();
    }

    @AfterEach
    public void endTest() {
        driver.quit();
    }


    private void buildDriver() throws IOException, JSONException {
        basePath = ((String) data.get("basePath")) + ":" + port;

        BasePage.basePath = basePath;
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
/*
        if (((String) data.get("browserName")).equals("chrome")) {

            ChromeOptions options = new ChromeOptions();
            if ((data.containsKey("headless")) && ((Boolean) data.get("headless"))) {
                options.addArguments("--headless");
            }
            driver = new ChromeDriver(options);
        }
        if ((data.containsKey("height")) && (data.containsKey("width"))) {
            driver.manage().window().setSize(new Dimension(
                    (Integer) data.get("width"),
                    (Integer) data.get("height")
            ));
        }
        if ((data.containsKey("wait"))) {
            driver.manage().timeouts().implicitlyWait(
                    (Integer) data.get("wait"),
                    TimeUnit.SECONDS);
        }*/

        js = (JavascriptExecutor) driver;
        ts = (TakesScreenshot) driver;
    }
}