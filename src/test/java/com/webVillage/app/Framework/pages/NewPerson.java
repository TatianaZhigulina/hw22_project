package com.webVillage.app.Framework.pages;

import org.openqa.selenium.WebDriver;

public class NewPerson extends BasePage{

    public NewPerson(WebDriver driver){
        this.driver = driver;
        this.path = "/person/new";
    }

    public NewPerson open(){
        driver.get(basePath + path);
        return this;
    }

    public NewPerson setFirstName(String firstName){
        getElementById("firstName").sendKeys(firstName);
        return this;
    }

    public NewPerson setSecondName(String secondName){
        getElementById("secondName").sendKeys(secondName);
        return this;
    }

    public NewPerson setAge(String age){
        getElementById("age").sendKeys(age);
        return this;
    }
}
