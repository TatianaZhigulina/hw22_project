package com.webVillage.app.Framework.tests;

import com.webVillage.app.Framework.helper.TestHelper;
import com.webVillage.app.Framework.pages.StartPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.OutputType;
import java.io.File;


public class PersonInfoTests extends TestHelper {

    @Test
    void NewPersonTesting() throws InterruptedException {

        new StartPage(driver)
                .open()
                .clickNewPerson()
                .setFirstName("fdsfdf")
                .setSecondName("fdgdfgd")
                .setAge("55");
        Thread.sleep(5000);
    }

    @Test
    void PersonInfoTesting() throws InterruptedException {
        new StartPage(driver)
                .open()
                .clickToJohnDow()
                .clickToEdit();
        Assertions.assertEquals("http://localhost:8080/edit/1", driver.getCurrentUrl());
    }

    @Test
    void screenExample(){
        driver.navigate().to("https://google.com");
        File screen = ts.getScreenshotAs(OutputType.FILE);
        File target = new File("C:\\Users\\Zhigu\\Java_course\\lecture_31\\webFun\\src\\main\\resources\\screen1.png");
        screen.renameTo(target);
    }
}
