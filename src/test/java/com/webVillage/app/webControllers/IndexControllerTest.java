package com.webVillage.app.webControllers;

import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.File;
import java.nio.file.Files;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

// https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/

//https://chromedriver.chromium.org/downloads

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class IndexControllerTest {

    private WebDriver driver;
    private String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\AlisaSkrynko\\Downloads\\chromedriver_win32\\chromedriver.exe");
    }

    @BeforeEach
    public void beforeTest(){
          driver = new ChromeDriver();
    }

    @AfterEach
    public void afterTest(){
        driver.quit();
    }

    @Test
    void cookieTesting() throws InterruptedException {
        driver.get(startPage + port + "/");
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println(cookies);
        Cookie cookie = driver.manage().getCookieNamed("authCode");
        System.out.println(cookie.getValue());
        Assertions.assertTrue(cookie.getValue().matches("[0-9]*"));

        driver.manage().addCookie(new Cookie("authCode", "ololo"));
        Cookie cookieNew = driver.manage().getCookieNamed("authCode");
        System.out.println("cookieNew " + cookieNew);

        driver.manage().deleteCookie(cookieNew);
        Set<Cookie> cookiesNew = driver.manage().getCookies();
        System.out.println(cookiesNew);
    }

    @Test
    void popupTesting() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(startPage + port + "/");
        String mainWindow = driver.getWindowHandle();

        driver.findElement(By.id("1")).click();
        driver.findElement(By.id("1")).click();
        driver.findElement(By.id("1")).click();

        Set<String> windows = driver.getWindowHandles();
        Thread.sleep(2000);

        for (String window:
             windows) {
            driver.switchTo().window(window);
            System.out.println(driver.getCurrentUrl());
            Thread.sleep(1000);
            if (!window.equals(mainWindow)){
                driver.findElement(By.id("popupButton")).click();
                Thread.sleep(1000);
                driver.close();
            }
        }

        Set<String> windowsNew = driver.getWindowHandles();
        for (String window:
                windowsNew) {
            driver.switchTo().window(window);
            Thread.sleep(1000);
            if (!window.equals(mainWindow)){
                Assertions.assertEquals("I am hiding",
                        driver.findElement(By.id("good")).getText());
                driver.close();
            }
        }
    }


    @Test
    void allertTesting() throws InterruptedException {
        driver.get(startPage +  port +  "/greeting");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        ///js.executeScript("alert('OLOLOLO')");
        //js.executeScript("document.check = confirm('OLOLOLO')");
        js.executeScript("document.check = prompt('OLOLOLO')");
        Thread.sleep(2000);
        driver.switchTo().alert().sendKeys("FROM TEST");
        driver.switchTo().alert().accept();

        Thread.sleep(1000);
        //Assertions.assertEquals("I am hiding", driver.findElement(By.id("good")).getText());
        //Boolean result = (Boolean) js.executeScript("return document.check");
        String result = (String) js.executeScript("return document.check");
        //Assertions.assertTrue(result);
        Assertions.assertEquals("FROM TEST", result);
    }


    @Test
    void greeting() throws InterruptedException {

        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String name = "Alisa";
        String surname = "Skrynko";
        driver.get(startPage +  port +  "/person/new");
        driver.findElement(By.id("firstName")).sendKeys(name);
        driver.findElement(By.id("secondName")).sendKeys(surname);
        driver.findElement(By.id("age")).sendKeys("18");
        List<WebElement> occupation = driver.findElements(By.id("occupation"));
        Random rnd = new Random();
        occupation.get(rnd.nextInt(occupation.size())).click();

        driver.findElement(By.cssSelector("select#gender"))
                .findElement(By.cssSelector("option[value='FEMALE']")).click();

        driver.findElement(By.id("doIt")).click();

        //Thread.sleep(5000);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        //WebElement element = driver.findElement(By.cssSelector("#nameLoaded"));

/*
        boolean elementIsVisible = wait.until(ExpectedConditions.textToBe(
                By.cssSelector("#name"), "NAME: " + name + " " + surname));
*/

      /*  WebElement loading = driver.findElement(By.cssSelector("#loadingForm"));
        wait.until(ExpectedConditions.and(ExpectedConditions.stalenessOf(loading),
                ExpectedConditions.not(
                        ExpectedConditions.textToBe(By.cssSelector("#name"), ""))));*/

        Wait<WebDriver> waitFluent = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                        .pollingEvery(Duration.ofMillis(100))
                .ignoring(NoSuchElementException.class);

        WebElement element = waitFluent.until(new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return driver.findElement(By.cssSelector("#nameLoaded"));
            }
        });


        //Assertions.assertTrue(elementIsVisible);
        Assertions.assertEquals("NAME: " + name + " " + surname, element.getText());

        //assertEquals(startPage + port + "/", driver.getCurrentUrl());

        System.out.println(driver.getPageSource());

/*        WebElement element = driver.findElement(By.id("age"));
        Assertions.assertTrue(element.isDisplayed());
        Assertions.assertTrue(element.isEnabled());

        element.sendKeys(Keys.CONTROL + "A");
        element.sendKeys(Keys.DELETE);  //equals element.clear();

        Thread.sleep(8000);*/

    }

    @Test
    void iFrameTesting() throws InterruptedException {
        driver.get(startPage + port + "/");
        driver.switchTo().frame("lol");
        WebElement element = driver.findElement(
                By.xpath("//h1[@class='text-center text-white']"));
        System.out.println(element.getText());
        driver.switchTo().parentFrame();
    }


    @Test
    void jsTesting() throws InterruptedException {
        driver.get(startPage + port + "/person/new");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //js.executeScript("document.body.innerHTML = '<h2>GFKKHGVHVK</h2>'");
/*        String str = "HELLO";
        js.executeScript("document.body.innerHTML = '<h2>' + arguments[0] + arguments[1] + '</h2>'",
                str, "!!!!!!!");*/
        WebElement element = driver.findElement(By.id("firstName"));
        Thread.sleep(1000);
        js.executeScript("arguments[0].value = 'FROM JS!!!'", element);
/*
        driver.manage().window().setSize(new Dimension(300, 300));
        Thread.sleep(5000);

        js.executeScript("arguments[0].scrollIntoView(); \n window.scrollBy(0, -50)", element);
        */

        //js.executeScript("window.location.replace('http://google.com')");
       // Thread.sleep(5000);

        String value = (String) js.executeScript("return arguments[0].value", element);
        System.out.println("VALUE: " + value);

        WebElement parent = (WebElement) js.executeScript("return arguments[0].parentElement", element);
        System.out.println("Parent: " + parent.getTagName());
    }

    @Test
    void screenExample(){
        TakesScreenshot ts = (TakesScreenshot) driver;
        driver.navigate().to("https://google.com");
        File screen = ts.getScreenshotAs(OutputType.FILE);
        File target = new File("D:\\testing\\AQA\\aqajunior\\webFun\\src\\test\\java\\com\\webVillage\\app\\webControllers\\screen.png");
        screen.renameTo(target);
    }
}